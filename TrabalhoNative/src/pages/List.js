import React, { Component } from 'react';
import api from '../services/api';
import styled from 'styled-components'
import { Alert, Text, FlatList } from 'react-native';

export default class Main extends Component {

    constructor({ navigation }) {
        super();
        this.navigation = navigation;
      }

    state = {
        cliente: [],
    };

    componentDidMount() {
        this.loadcliente();
    };

    componentDidUpdate() {
        this.loadcliente();
    }

    loadcliente = async (page = 1) => {
        const response = await api.get(`/users`);
        const {data} = response;
        
        this.setState({cliente: data});
    };

    RemoveCliente = async (id) => {
        const user = await api.delete(`/users/${id}`)
        Alert.alert(`O usuario ${user.data.name} foi deletado.`)
        this.loadcliente()
    }

    render() {
        const clientes = this.state.cliente;
        return (
            <Layout>
                <FlatList
                data={clientes}
                ListEmptyComponent={<EmptyMenssage>Não há tarefas Castradas</EmptyMenssage>}
                keyExtractor={(item) => item.email}
                renderItem={({item}) => 
                <Container key={item.id}>
                    <Text>Nome: {item.name}</Text>
                    <Text>email: {item.email}</Text>
                    <BtnContainer>
                        <Btn onPress={() => this.navigation.navigate("UpdateUser", item.id)}>
                            <Text>Aterar</Text>
                        </Btn>
                        <Btn onPress={() => this.RemoveCliente(item.id)}>
                            <Text>Delete</Text>
                        </Btn>
                    </BtnContainer>
                </Container>} />
            </Layout>     
        )
    };
}

const Layout = styled.View`
    background-color: #d9d6e5;
    border: 2px solid #444380;
    align-items: center;
    justify-content: center;
`

const Container = styled.View`
  background-color: #d9d6e5;
  align-items: center;
  border: 2px solid #444380;
  border-radius: 5px;
  margin: 10px;
`;

const BtnContainer = styled.View`
  flex-direction: row;
`

const Btn = styled.TouchableOpacity`
  margin: 0px 8px;
  border: 2px solid #444380;
  border-radius: 5px;
  padding: 2px;
`;

const EmptyMenssage = styled.Text`
color: #444380;
align-items: center;
justify-content: center;
`;
