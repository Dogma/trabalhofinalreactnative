import React, { Component } from "react";
import api from "../services/api";
import styled from "styled-components";
import { Alert } from "react-native";

export default class UpdateUser extends Component {

  constructor(props) {
    super();
    this.userId = props.navigation.state.params;
    this.navigation = props.navigation;
    
    this.getUser();
  }

  state = {
    name: "",
    email: "",
    password_hash: "",
  };

  async getUser() {
    const user = await api.get(`/users/${this.userId}`)
    const {name, email, password_hash} = user.data
    this.setState({name, email, password_hash})
  }

  enviar = async () => {
    const user = await api.put(`/users/${this.userId}`, this.state);
    if (user) {
      Alert.alert(`Cliente ${user.data.name} Alterado`);
      this.navigation.navigate("List")
    } else {
      Alert.alert(`Não foi possivel Alterado`);
    }  
  };

  render() {
    return (
      <Conteiner>
        <Title>Informações Pessoais</Title>
        <Label>Nome</Label>
        <Input onChangeText={text => this.setState({ name: text })}
        value={this.state.name} />
        <Label>E-mail</Label>
        <Input onChangeText={text => this.setState({ email: text })}
        value={this.state.email} />
        <Label>Senha</Label>
        <Input onChangeText={text => this.setState({ password_hash: text })}
        value={this.state.password_hash} />
        <BtnEnviar onPress={() => this.enviar()}>
          <BtnLabel>Entrar</BtnLabel>
        </BtnEnviar>
      </Conteiner>
    );
  }
}

const Conteiner = styled.View`
  flex: 1;
  background-color: #d9d6e5;
  align-items: center;
`;

const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  margin-top: 18px;
  margin-bottom: 10px;
  color: #444380;
`;

const Label = styled.Text`
  margin-top: 10px;
  font-size: 16px;
  font-weight: bold;
  color: #444380;
`;

const Input = styled.TextInput`
  width: 90%;
  border: 2px solid #444380;
  border-radius: 5px;
  padding: 5px;
`;

const BtnEnviar = styled.TouchableOpacity`
  margin-top: 30px;
  border: 2px solid #444380;
  border-radius: 5px;
  padding: 10px;
`;

const BtnLabel = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #444380;
`;
