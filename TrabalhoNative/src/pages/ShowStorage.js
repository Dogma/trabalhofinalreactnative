import React, { Component} from "react";
import styled from "styled-components";
import { AsyncStorage } from 'react-native'
import {Image} from 'react-native'

export default class ShowStorage extends Component {

  constructor({ navigation }) {
    super();
    this.getStorageImg();
    this.navigation = navigation;
  }

  state = {
    base64: ''
  }

  getStorageImg = async () => {
    const img = await AsyncStorage.getItem('img')
    this.setState({base64: img})
    this.render();
  }
  
  render() {
    const img64 = `data:image/jpg;base64,${this.state.base64}`;
    return (
      <Container>
        <Image style={{
            width: 200,
            height: 300,
            resizeMode: 'contain',
          }} source={{uri: img64 }} />
        <WellCome>Sua Foto</WellCome>
        <BtnVoltar onPress={() => this.navigation.navigate("Menu")}>
          <WellCome>Voltar</WellCome>
        </BtnVoltar>
      </Container>
    );
  }
  
}

const Container = styled.View`
  flex: 1;
  background-color: #d9d6e5;
  align-items: center;
`;

const Logo = styled.Image`
  width: 100%;
  max-height: 300px;
`;

const WellCome = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #444380;
`;

const InputName = styled.TextInput`
  margin-top: 15px;
  border: 2px solid #444380;
  min-width: 200px;
  padding: 1px;
`;

const BtnVoltar = styled.TouchableOpacity`
  margin-top: 15px;
  border: 2px solid #444380;
  border-radius: 5px;
  padding: 5px;
`;
