import React, { Component } from 'react';
import { Button, Image, Text, View, StyleSheet, AsyncStorage } from 'react-native';
import { ImagePicker, Constants } from 'expo';

export default class App extends Component {
  state = {
    pickerResult: null,
  };

  _pickImg = async () => {
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      base64: true,
      allowsEditing: false,
      aspect: [4, 3],
    });

    this.setState({
      pickerResult,
    });
    await AsyncStorage.setItem('img', this.state.pickerResult.base64)
  };

  render() {
    let { pickerResult } = this.state;
    let imageUri = pickerResult ? `data:image/jpg;base64,${pickerResult.base64}` : null;
    imageUri && console.log({uri: imageUri.slice(0, 100)});
    
    return (
      <View style={styles.container}>
        <Button onPress={this._pickImg} title="Open Picker" />
        {pickerResult
          ? <Image
              source={{uri: imageUri}}
              style={{ width: 200, height: 200 }}
            />
          : null}
        {pickerResult
          ? <Text style={styles.paragraph}>
              Keys on pickerResult:
              {' '}
              {JSON.stringify(Object.keys(pickerResult))}
            </Text>
          : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
});