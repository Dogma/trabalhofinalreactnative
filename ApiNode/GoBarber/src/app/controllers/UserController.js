import User from '../models/User';

class UserController {
  async store(req, res) {
    const userExists = await User.findOne({ where: { email: req.body.email } });
    if (userExists) {
      return res.status(400).json({ error: 'User already exists' });
    }
    const { id, name, email, provider } = await User.create(req.body);
    return res.json({ id, name, email, provider });
  }

  async get(req, res) {
    const users = await User.findAll();
    return res.json(users);
  }

  async getOne(req, res) {
    const user = await User.findOne({ where: { id: req.params.id } });
    return res.json(user);
  }

  async delete(req, res) {
    const user = await User.findOne({ where: { id: req.params.id } });
    User.destroy({ where: { id: req.params.id } });
    return res.json(user.dataValues);
  }

  async update(req, res) {
    const user = await User.findOne({ where: { id: req.params.id } });
    if (user) {
      User.update(req.body, { where: { id: req.params.id } });
      return res.json(user.dataValues);
    }
    return res.status(500);
  }
}

export default new UserController();
